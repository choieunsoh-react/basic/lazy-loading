import React, { Suspense, lazy } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes as Switch,
  Link,
} from "react-router-dom";
import "./App.css";

const UserList = lazy(() => import("./components/UserList"));

function App() {
  return (
    <Router>
      <Suspense
        fallback={<div style={{ textAlign: "center" }}>Loading...</div>}
      >
        <Switch>
          <Route path="/user-list" element={<UserList />} />
          <Route
            path="/"
            element={
              <div style={{ textAlign: "center" }}>
                <h1>Home Page</h1>
                <Link to="/user-list">User List</Link>
              </div>
            }
          />
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
