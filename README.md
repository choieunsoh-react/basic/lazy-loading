# Set up Lazy Loading in React Application

## Advantages of Using Lazy Loading

- Implementation of lazy loading in ReactJS is very easy.
- Unwanted code execution can be avoided.
- Minimum usage of time and memory makes it a cost-effective approach and improves the user experience.

## Disadvantages of Using Lazy Loading

- It may affect the website ranking on search engines.
- Extra line of code is added.

## Syntax

```js
const UserListComponent = React.lazy(() => import("./UserListComponent"));
```

https://pankaj-kumar.medium.com/set-up-lazy-loading-in-react-application-ed1888a8c671
